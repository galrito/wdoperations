/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
This file shows an example of implementing the OperationCondition protocol.
*/

#if os(iOS)

import UIKit
    
private let RemoteNotificationQueue = OperationQueue()
    
public extension Notification.Name {
    fileprivate static let remoteNotificationName = Notification.Name(rawValue: "RemoteNotificationPermissionNotification")
}

private enum RemoteRegistrationResult {
    case token(NSData)
    case error(NSError)
}

/// A condition for verifying that the app has the ability to receive push notifications.
public struct WDRemoteNotificationCondition: WDOperationCondition {
    public static let name = "RemoteNotification"
    public static let isMutuallyExclusive = false
    
    public static func didReceiveNotificationToken(token: Data) {
        NotificationCenter.default.post(name: .remoteNotificationName, object: nil, userInfo: [
            "token": token
        ])
    }
    
    public static func didFailToRegister(error: NSError) {
        NotificationCenter.default.post(name: .remoteNotificationName, object: nil, userInfo: [
            "error": error
        ])
    }
    
    public let application: UIApplication
    
    public init(application: UIApplication) {
        self.application = application
    }
    
    public func dependencyForOperation(_ operation: WDOperation) -> Operation? {
        return RemoteNotificationPermissionOperation(application: application, handler: { _ in })
    }
    
    public func evaluateForOperation(_ operation: WDOperation, completion: @escaping (WDOperationConditionResult) -> Void) {
        /*
            Since evaluation requires executing an operation, use a private operation
            queue.
        */
        RemoteNotificationQueue.addOperation(RemoteNotificationPermissionOperation(application: application) { result in
            switch result {
                case .token(_):
                    completion(.satisfied)

                case .error(let underlyingError):
                    let error = NSError(code: .conditionFailed, userInfo: [
                        WDOperationConditionKey: type(of: self).name,
                        NSUnderlyingErrorKey: underlyingError
                    ])

                    completion(.failed(error))
            }
        })
    }
}

/**
    A private `Operation` to request a push notification token from the `UIApplication`.
    
    - note: This operation is used for *both* the generated dependency **and**
        condition evaluation, since there is no "easy" way to retrieve the push
        notification token other than to ask for it.

    - note: This operation requires you to call either `RemoteNotificationCondition.didReceiveNotificationToken(_:)` or
        `RemoteNotificationCondition.didFailToRegister(_:)` in the appropriate
        `UIApplicationDelegate` method, as shown in the `AppDelegate.swift` file.
*/
private class RemoteNotificationPermissionOperation: WDOperation {
    let application: UIApplication
    private let handler: (RemoteRegistrationResult) -> Void
    
    fileprivate init(application: UIApplication, handler: @escaping (RemoteRegistrationResult) -> Void) {
        self.application = application
        self.handler = handler

        super.init()
        
        /*
            This operation cannot run at the same time as any other remote notification
            permission operation.
        */
        addCondition(WDMutuallyExclusive<RemoteNotificationPermissionOperation>())
    }
    
    override func execute() {
        DispatchQueue.main.async {
            let notificationCenter = NotificationCenter.default
            
            notificationCenter.addObserver(self, selector: #selector(self.didReceiveResponse(_:)), name: .remoteNotificationName, object: nil)
            
            self.application.registerForRemoteNotifications()
        }
    }
    
    @objc func didReceiveResponse(_ notification: NSNotification) {
        NotificationCenter.default.removeObserver(self)
        
        let userInfo = notification.userInfo

        if let token = userInfo?["token"] as? NSData {
            handler(.token(token))
        }
        else if let error = userInfo?["error"] as? NSError {
            handler(.error(error))
        }
        else {
            fatalError("Received a notification without a token and without an error.")
        }

        finish()
    }
}
    
#endif
